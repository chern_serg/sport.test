<?php

namespace Test\SiteBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

class ProductRepository extends EntityRepository
{
    /**
     * Returns products info.
     * @return mixed
     */
    public function getProductsInfo()
    {
        $em = $this->getEntityManager();

        $queryBuilder = $em->createQueryBuilder()
            ->select('Product')
            ->from('TestSiteBundle:Product', 'Product')
            ->orderBy('Product.id', 'ASC');

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Get products table headers.
     *
     * @return array
     */
    public function getProductsListHeaders()
    {
        return array('id', 'Title', 'Description', 'Amount', 'Price', 'Discount', 'Actions');
    }
}
