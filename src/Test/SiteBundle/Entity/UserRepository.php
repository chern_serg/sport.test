<?php

namespace Test\SiteBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

class UserRepository extends EntityRepository
{
    /**
     * Returns users info.
     * @return mixed
     */
    public function getUsersInfo()
    {
        $em = $this->getEntityManager();

        $queryBuilder = $em->createQueryBuilder()
            ->select('User', 'SUM(Purchase.totalDiscPrice) as total_amount', 'Count(Purchase.id) as purch_amount')
            ->from('TestSiteBundle:User', 'User')
            ->leftJoin('User.purchases', 'Purchase')
            ->groupBy('User.id')
            ->orderBy('User.fullName', 'ASC');

//        print_r($queryBuilder->getQuery()->getSQL());
//        exit;

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Get products table headers.
     *
     * @return array
     */
    public function getUsersListHeaders()
    {
        return array('Full Name', 'Email', 'Phone', 'Birthday', 'City', 'Purchases', 'Total amount', 'Actions');
    }
}
