<?php

namespace Test\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Intl\Intl;
use Test\SiteBundle\Services\Object;
use Test\SiteBundle\Entity\Purchase;

/**
 * @ORM\Entity(repositoryClass="Test\SiteBundle\Entity\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(
 *     fields={"email"},
 *     message="This email is already in use."
 * )
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="full_name", length=255)
     * @Assert\NotBlank(message="Full name is required")
     */
    protected $fullName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(message="The email {{ value }} is not a valid email.")
     * @Assert\NotBlank(message="Email is required")
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank(message="Phone is required")
     */
    protected $phone;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="Date of birth is required")
     */
    protected $dob;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\Choice(callback = "getAllowedCitiesKeys", message = "Incorrect value for chapter status.")
     * @Assert\NotBlank(message ="City is required")
     */
    protected $city;

    /**
     * @ORM\OneToMany(targetEntity="Test\SiteBundle\Entity\Purchase", mappedBy="user", cascade={"remove"})
     */
    protected $purchases;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullName.
     *
     * @param string $fullName
     * @return User
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName.
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set email.
     *
     * @param $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone.
     *
     * @param $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set dob.
     *
     * @param \DateTime $dob
     * @return User
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob.
     *
     * @return \DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set city.
     *
     * @param $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Return list of allowed statuses
     *
     * @return array
     */
    public static function getAllowedCities()
    {
        return array(
            'kyiv' => 'Kyiv',
            'dnipropetrovsk' => 'Dnipropetrovsk',
            'kharkiv' => 'Kharkiv',
            'kherson' => 'Kherson',
            'lviv' => 'Lviv',
            'odesa' => 'Odesa',
            'poltava' => 'Poltava',
            'rivne' => 'Rivne',
            'simferopol' => 'Simferopol',
            'vinnytsia' => 'Vinnytsia',
        );
    }

    /**
     * Return list of allowed Ukrainian cities keys
     * @return array
     */
    public static function getAllowedCitiesKeys()
    {
        return array_keys(self::getAllowedCities());
    }

    /**
     * Get purchases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * Add purchase
     *
     * @param Purchase $purchase
     * @return User
     */
    public function addPurchase(Purchase $purchase)
    {
        $this->purchases[] = $purchase;

        return $this;
    }

    /**
     * Remove purchase
     *
     * @param Purchase $purchase
     */
    public function removePurchase(Purchase $purchase)
    {
        $this->purchases->removeElement($purchase);
    }
}
