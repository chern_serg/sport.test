<?php

namespace Test\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Intl\Intl;
use Test\SiteBundle\Services\Object;

/**
 * @ORM\Entity(repositoryClass="Test\SiteBundle\Entity\ProductRepository")
 * @ORM\Table(name="product")
 * @UniqueEntity(
 *     fields={"title"},
 *     message="This title is already in use."
 * )
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=128, nullable=false)
     * @Assert\NotBlank(message ="Title is required.")
     */
    protected $title;

    /**
     * @ORM\Column(type="text", length=512, nullable=false)
     * @Assert\NotBlank(message ="Description is required.")
     */
    protected $description;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank(message ="Amount is required.")
     */
    protected $amount;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank(message ="Price is required.")
     */
    protected $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $discount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $number;

    /**
     * @ORM\OneToMany(targetEntity="Test\SiteBundle\Entity\Purchase", mappedBy="product", cascade={"remove"})
     */
    protected $purchases;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set amount.
     *
     * @param integer $amount
     * @return Product
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set price.
     *
     * @param integer $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set discount.
     *
     * @param integer $discount
     * @return Product
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount.
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set number.
     *
     * @param integer $number
     * @return Product
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get product purchases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * Add purchase
     *
     * @param Purchase $purchase
     * @return Product
     */
    public function addPurchase(Purchase $purchase)
    {
        $this->purchases[] = $purchase;

        return $this;
    }

    /**
     * Remove purchase
     *
     * @param Purchase $purchase
     */
    public function removePurchase(Purchase $purchase)
    {
        $this->purchases->removeElement($purchase);
    }

    /**
     * Get formatted price.
     *
     * @return integer
     */
    public function getFormattedPrice()
    {
        return number_format($this->price / 100, 2, '.', '');
    }
}
