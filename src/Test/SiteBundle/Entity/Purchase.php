<?php

namespace Test\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Intl\Intl;
use Test\SiteBundle\Services\Object;
use Test\SiteBundle\Entity\User;
use Test\SiteBundle\Entity\Product;

/**
 * @ORM\Entity()
 * @ORM\Table(name="purchase")
 */
class Purchase
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Test\SiteBundle\Entity\User", inversedBy="user")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message="User is required")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Test\SiteBundle\Entity\Product", inversedBy="products")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    protected $product;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank(message ="Amount is required.")
     */
    protected $amount;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\NotBlank(message ="Created is required.")
     */
    protected $created;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $price;

    /**
     * @ORM\Column(type="integer", name="total_disc_price", nullable=true)
     */
    protected $totalDiscPrice;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreated(Object::getCurrentTime());
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user.
     *
     * @param User $user
     * @return Purchase
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set product.
     *
     * @param Product $product
     * @return Purchase
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set amount.
     *
     * @param integer $amount
     * @return Purchase
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount of products.
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set created
     *
     * @param $created
     * @return Purchase
     */
    private function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return /DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set price.
     *
     * @param integer $price
     * @return Purchase
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price of 1 product.
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }


    /**
     * Set total discount price.
     *
     * @param integer $totalDiscPrice
     * @return Purchase
     */
    public function setTotalDiscPrice($totalDiscPrice)
    {
        $this->totalDiscPrice = $totalDiscPrice;

        return $this;
    }

    /**
     * Get total discount price.
     *
     * @return integer
     */
    public function getTotalDiscPrice()
    {
        return $this->totalDiscPrice;
    }

    /**
     * Get total price.
     *
     * @return integer
     */
    public function getTotalPrice()
    {
        return $this->price * $this->amount;
    }

    /**
     * Get formatted total price.
     *
     * @return integer
     */
    public function getFormattedTotalPrice()
    {
        return number_format($this->price * $this->amount / 100, 2, '.', '');
    }
    /**
     * Get formatted total discount price.
     *
     * @return integer
     */
    public function getFormattedTotalDiscPrice()
    {
        return number_format($this->totalDiscPrice / 100, 2, '.', '');;
    }
}
