(function ($) {
    var $type = $('#object_type');
    $type.change(function () {
        $.ajax({
            type: 'GET',
            url: 'get-object-form/' + $type.val(),
            success: function (html) {
                $('#object_form').html(html);
            }
        });
    });

//    var $type = $('#object_type');
//    $(".object-list-table").on('click','.btn-delete',function(){
//        event.preventDefault();
//        console.log(this);
//
////        $(this).closest('tr').remove();
//    });

    $(".btn-object-delete").click(
        function(event) {
            event.preventDefault();
            var $btn = $(this);

            $.ajax({
                type: 'POST',
                url: $btn.closest('table').attr('id') + '/delete',
                data: { id: $(this).attr('id').slice(4) },
                success: function(data){
                    if(data === 'success'){
                        $btn.closest('tr').remove();
                    }else{
                        alert("Impossible to delete this object! Response: " + data)
                    }
                }
            })
        }
    );
})(jQuery);
