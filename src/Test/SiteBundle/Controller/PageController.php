<?php

namespace Test\SiteBundle\Controller;

use Doctrine\ORM\ORMException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Test\SiteBundle\Entity\Purchase;
use Test\SiteBundle\Entity\User;
use Test\SiteBundle\Form\UserType;
use Test\SiteBundle\Services\Object;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Test\SiteBundle\Entity\Product;
use Doctrine\ORM\EntityManager;

class PageController extends Controller
{
    // @todo Show form errors.

    /**
     * View front page.
     *
     * @Route("/", name="site_front_page")
     */
    public function frontPageAction(Request $request)
    {
        return $this->render('TestSiteBundle:Pages:front.html.twig');
    }

    /**
     * View add object page.
     *
     * @Method("GET");
     * @Route("/add-object", name="add_object_page")
     */
    public function addObjectPageAction(Request $request)
    {
        return $this->render('TestSiteBundle:Pages:addObject.html.twig', array(
            'options' => Object::getObjectTypes(),
            'form' => $this->getObjectForm(Object::DEFAULT_TYPE)->getContent(),
        ));
    }

    /**
     * View add object page.
     *
     * @Method("POST");
     * @Route("/add-object", name="add_object")
     */
    public function addObjectAction(Request $request)
    {
        if (!isset($request->request->keys()[0])) {
            throw new \Exception('Incorrect form data.');
        }
        $formAttr = Object::getFormAttrByName($request->request->keys()[0]);

        $form = $this->createForm($formAttr['form_type'], $formAttr['entity'])->submit($request);


        $this->prepareObject($formAttr['entity']);

        if ($form->isValid()) {
            $request->getSession()->getFlashBag()->add('success', 'Object has been created!');
            $em = $this->getDoctrine()->getManager();
            $em->persist($formAttr['entity']);
            $em->flush();

            return $this->redirect($this->generateUrl('add_object_page'));
        }

        return $this->render('TestSiteBundle:Pages:addObject.html.twig', array(
            'options' => Object::getObjectTypes(),
            'form' => $this->getObjectForm(Object::DEFAULT_TYPE)->getContent(),
        ));
    }

    /**
     * Get object form.
     */
    public function prepareObject($object)
    {
        if ($object instanceof Purchase) {
            $object->setPrice($object->getProduct()->getPrice());
            if ($object->getAmount() >=  $object->getProduct()->getNumber()) {
                $object->setTotalDiscPrice($object->getTotalPrice() - round($object->getTotalPrice() * $object->getProduct()->getDiscount()/100));
            }
        } elseif ($object instanceof Product) {
            $object->setPrice($object->getPrice()*100);
        }
    }

    /**
     * Get object form.
     *
     * @Method("GET");
     * @Route("/get-object-form/{name}", name="get_object_form")
     */
    public function getObjectForm($name)
    {
        $formAttr = Object::getFormAttrByName($name);

        return $this->render('TestSiteBundle:Forms:object.html.twig', array(
            'object_form' => $this->createForm($formAttr['form_type'], $formAttr['entity'])->createView(),
            'classSuccess' => '',
        ));
    }

    /**
     * View users list.
     *
     * @Method("GET");
     * @Route("/users", name="users_list")
     */
    public function usersListPageAction(Request $request)
    {
        $userRepo = $this->getDoctrine()->getManager()->getRepository("TestSiteBundle:User");

        return $this->render('TestSiteBundle:Pages:users.html.twig', array(
            'headers' => $userRepo->getUsersListHeaders(),
            'users' => $userRepo->getUsersInfo(),
        ));
    }

    /**
     * Delete user action.
     *
     * @Method("POST");
     * @Route("/user/delete", name="delete_user")
     */
    public function userDeleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("TestSiteBundle:User")->find($request->get('id'));
        if ($user instanceof User) {
            try {
                $em->remove($user);
                $em->flush();

                return new Response('success');
            } catch (\Exception $e) {
                return  new Response($e->getMessage());
            }
        }

        return new Response('Not found User.');
    }

    /**
     * View products list.
     *
     * @Method("GET");
     * @Route("/products", name="products_list")
     */
    public function productsListPageAction(Request $request)
    {
        $productRepo = $this->getDoctrine()->getManager()->getRepository("TestSiteBundle:Product");

        return $this->render('TestSiteBundle:Pages:products.html.twig', array(
            'headers' => $productRepo->getProductsListHeaders(),
            'products' => $productRepo->getProductsInfo(),
        ));
    }

    /**
     * Delete product action.
     *
     * @Method("POST");
     * @Route("/product/delete", name="delete_product")
     */
    public function productDeleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository("TestSiteBundle:Product")->find($request->get('id'));
        if ($product instanceof Product) {
            try {
                $em->remove($product);
                $em->flush();

                return new Response('success');
            } catch (\Exception $e) {
                return  new Response($e->getMessage());
            }
        }

        return new Response('Not found Product.');
    }

//    /**
//     * View purchases list.
//     *
//     * @Route("/purchases", name="purchases_list")
//     */
//    public function purchasesListPageAction(Request $request)
//    {
//        return $this->render('TestSiteBundle:Pages:front.html.twig');
//    }
}
