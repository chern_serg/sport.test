<?php

namespace Test\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use Test\SiteBundle\Services\Object;
use Test\SiteBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\QueryBuilder;

class PurchaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'entity', array(
                'class' => 'TestSiteBundle:User',
                'query_builder' => function ($repository) {
                    return $repository->createQueryBuilder('u')->orderBy('u.fullName', 'ASC');
                },
                'property' => 'fullName',
            ))
            ->add('product', 'entity', array(
                'class' => 'TestSiteBundle:Product',
                'query_builder' => function ($repository) {
                    return $repository->createQueryBuilder('p')->orderBy('p.title', 'ASC');
                },
                'property' => 'title',
            ))
            ->add('amount', 'number', array(
                'label' => 'Amount: ',
            ))
            ->add('save', 'submit', array('label' => 'Create'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Test\SiteBundle\Entity\Purchase'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'purchase';
    }
}