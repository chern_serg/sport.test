<?php

namespace Test\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use Test\SiteBundle\Entity\User;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', 'text', array(
                'label' => 'Full name: ',
            ))
            ->add('email', 'text', array(
                'label' => 'Email: ',
            ))
            ->add('phone', 'text', array(
                'label' => 'Phone: ',
            ))
            ->add('dob', 'birthday', array(
                'label' => 'Date of Birth: ',
                'years' => range(1902, date('Y')),
            ))
            ->add('city', 'choice', array(
                'label' => 'City: ',
                'choices' => User::getAllowedCities(),
            ))
            ->add('save', 'submit', array('label' => 'Create'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Test\SiteBundle\Entity\User'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'user';
    }
}