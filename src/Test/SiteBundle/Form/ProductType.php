<?php

namespace Test\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use Test\SiteBundle\Services\Object;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Title: ',
            ))
            ->add('description', 'text', array(
                'label' => 'Description: ',
            ))
            ->add('amount', 'number', array(
                'label' => 'Amount: ',
            ))
            ->add('price', 'number', array(
                'label' => 'Price: ',
            ))
            ->add('discount', 'number', array(
                'label' => 'Discount: ',
            ))
            ->add('number', 'number', array(
                'label' => 'From: ',
            ))
            ->add('save', 'submit', array('label' => 'Create'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Test\SiteBundle\Entity\Product'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'product';
    }
}