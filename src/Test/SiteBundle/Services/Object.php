<?php

namespace Test\SiteBundle\Services;

use DateTime;
use Test\SiteBundle\Entity\Purchase;
use Test\SiteBundle\Entity\Product;
use Test\SiteBundle\Entity\User;
use Test\SiteBundle\Form\PurchaseType;
use Test\SiteBundle\Form\ProductType;
use Test\SiteBundle\Form\UserType;

/**
 * Class Object
 */
class Object
{
    const DEFAULT_TYPE = 'user';
    static private $currentTime;

    /**
     * Get current time.
     *
     * @return DateTime
     */
    public static function getCurrentTime()
    {
        if (empty(self::$currentTime)) {
            self::$currentTime = new \DateTime();
        }

        return clone self::$currentTime;
    }

    /**
     * Get available object types.
     *
     * @return array
     */
    public static function getObjectTypes()
    {
        return array(
            self::DEFAULT_TYPE => 'User',
            'product' => 'Product',
            'purchase' => 'Purchase'
        );
    }

    /**
     * Get form by name.
     *
     * @param string $name
     * @return array
     * @throws \Exception
     */
    public static function getFormAttrByName($name)
    {
        switch ($name) {
            case 'user':
                $entity = new User();
                $formType = new UserType();
                break;
            case 'product':
                $entity = new Product();
                $formType = new ProductType();
                break;
            case 'purchase':
                $entity = new Purchase();
                $formType = new PurchaseType();
                break;
            default:
                throw new \Exception('Incorrect form name.');
        }

        return array(
            'entity' => $entity,
            'form_type' => $formType
        );
    }
}
